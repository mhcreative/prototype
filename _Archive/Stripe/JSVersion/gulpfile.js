var gulp = require("gulp"),
    watch = require("gulp-watch"),
    opn = require('opn'),
    util = require('gulp-util'),
    del = require('del'),
    plumber = require('gulp-plumber'),
    minifyCSS = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    gulpIf = require('gulp-if'),
    browserSync = require('browser-sync').create();


// Config
var rootDir = __dirname,
    port = '8080',
    browsers = ['chrome', 'safari' , 'firefox'];

/**
* Browser Check
* @description This function check the browser passes as an argument from the Command Line and correct it if not in the browser arrray
*/
function browserCheck () {
  if ( browsers.indexOf(util.env.browser) < 0) { util.env.browser = 'google chrome'}
  if (util.env.browser == 'chrome') { util.env.browser = 'google chrome'};
}


gulp.task('console', function () {
  console.log('it works');
});

// Static Server + watching scss/html files
gulp.task('serve', function() {

    browserSync.init({
        server: rootDir
    });

    //gulp.watch("app/scss/*.scss", ['sass']);
    gulp.watch("index.html").on('change', browserSync.reload);
    gulp.watch(['**/*.js', '*.js'], ['scripts']);
    gulp.watch(['**/*.css', '*.css'], ['styles']);
});

// Gulp Script task for to watch all js files and reloading Connect server
gulp.task('scripts', function () {
  return gulp.src(['js/*.js', '*.js'])
    .pipe(plumber())
    .pipe(browserSync.stream({match: '**/*.js'}));
});

gulp.task('styles', function () {
  return gulp.src(['css/*.css', '*.css'])
    .pipe(plumber())
    .pipe(browserSync.stream({match: '**/*.css'}));
});

/* Copying */
gulp.task('copy:imgs', ['build:clean'] , function () {
  var stream =  gulp.src(['**/img/*.png', '**/*.jpg', '**/*.jpeg']) //, '**/*.jpg', '**/*.jpeg'
    .pipe(gulp.dest('build'));


    return stream;
});


gulp.task('copy:css', ['build:clean'] , function () {
  var stream =  gulp.src(['**/css/*.css', '*.css'])
             .pipe(minifyCSS())
             .pipe(gulp.dest('build'));
  return stream;
});

gulp.task('copy:html', ['build:clean'] , function () {
  var stream =  gulp.src(['**/*.html'])
    .pipe(gulp.dest('build'));

    return stream;
});

gulp.task('copy:js', ['build:clean'] , function () {
  var stream =  gulp.src(['**/js/*.js'])
                    .pipe(gulpIf('!holden.js', uglify()))
                    .pipe(gulp.dest('build'));

  return stream;
});

gulp.task('good', ['copy:css' , 'copy:imgs', 'copy:html', 'copy:js' ], function () {

});

gulp.task('build:clean', function () {
  return del(['build']).then(function (paths) {
    console.log('Deleted files/folders:\n', paths.join('\n'));
  });
  //return gulp.src('build', {read: false}).pipe(clean());
});


gulp.task('default', ['serve']);
