import React from 'react';
import ReactDOM from 'react-dom';


import AppFront from "./components/AppFront.js";

const app = document.getElementById('app');
ReactDOM.render(<AppFront />, app);
