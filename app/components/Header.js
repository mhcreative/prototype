import React from 'react';

import NavigationMenu from './NavigationMenu';
import Social from './Social';
import CartButton from './CartButton.js';

export default class Header extends React.Component{
  render(){
    return(
      <div className="header">
        <div className="inner">
          <Social/>

          <NavigationMenu homeUrl={this.props.homeUrl} galleryUrl={this.props.galleryUrl} aboutUrl={this.props.aboutUrl} contactUrl={this.props.contactUrl} />

          <CartButton cartCount={this.props.cartCount}
          cartClickhandler={this.props.cartClickhandler.bind(this)}
          loginButtonHandler={this.props.loginButtonHandler.bind(this)}
          accountClickHandler={this.props.accountClickHandler.bind(this)}
          signedIn={this.props.signedIn}
          />
        </div>
      </div>
    );
  }
}
