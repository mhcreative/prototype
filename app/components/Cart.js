import React from 'react';
import Checkout from "./Checkout.js";

export default class Cart extends React.Component{

  openModal(){
    // Get the modal
    var modal = document.getElementById('myModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
        modal.style.display = "block";

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            // modal.style.display = "none";
            this.props.cartClickhandler();
        }
    };

  };

  closeModal(){
      var modal = document.getElementById('myModal');
      // modal.style.display = "none";
      this.props.cartClickhandler();
  };

  quantityAdd(item){
    item.quantity++
    this.props.hackforceReload();
    this.props.cartCountIncrease();
  };
  quantityMinus(item,index){
    if(item.quantity === 1){
      this.props.cartDeleteHandler(index);
    }else{
      item.quantity--
      this.props.cartCountDecrease(item);
    };
    this.props.hackforceReload();
  };

  render(){

    var listCartItems =
    this.props.cartLoad.map((item,index)=>{
      return(
        <li key={index} className="cartItem">
        {item.name}<br/>
        {item.price}<br/>
        <button onClick={this.quantityMinus.bind(this,item,index)}>-</button><span className="quantityCount">{item.quantity}</span><button onClick={this.quantityAdd.bind(this,item)}>+</button>
        <button className="cartDelete" onClick={this.props.cartDeleteHandler.bind(this,index)}>Delete this item</button>
        </li>
      )
    });

    return(
      <div>

        <div id="myModal" className={this.props.cartHidden ?'modal hidden' : 'modal'}>

          <div className="modal-content">
            <span onClick={this.closeModal.bind(this)}className="close">x</span>
            <div>
              {this.props.cartCount > 0 ? <span>This is the cart</span> : <span>Nothing in the cart</span>}
            </div>
            {listCartItems}

            {this.props.cartCount === 0 ? null :
            <button onClick={this.props.checkoutClickHandler.bind(this)}>{this.props.checkoutButton}</button>
            }

            {
              this.props.checkoutHidden ? null :
              <Checkout
                cartHidden={this.props.cartHidden}
                checkoutDisabled={this.props.checkoutDisabled}
                cartLoad={this.props.cartLoad}
                checkoutHidden={this.props.checkoutHidden}
                addOrderHandler={this.props.addOrderHandler.bind(this)}
                userIdState={this.props.userIdState}
              />
            }

          </div>

        </div>

      </div>

    );
  }
}
