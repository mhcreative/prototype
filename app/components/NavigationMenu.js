import React from 'react';
var database = firebase.database();
export default class NavigationMenu extends React.Component{
  constructor(){
    super();
    database.ref("navigation").on('value',snap => {
      let home = snap.val().home;
    });
  };
  render(){
    return(
      <ul className="nav-bar">
        <div className="logo"><div><img src="./img/logo.png"/></div></div>
        <li><a href={this.props.homeUrl} target="_blank">shop</a></li>
        <li><a href={this.props.galleryUrl} target="_blank">gallery</a></li>
        <li><a href={this.props.aboutUrl} target="_blank">about</a></li>
        <li><a href={this.props.contactUrl} target="_blank">contact</a></li>
      </ul>
    );
  }
}
