import React from 'react';

export default class CartButton extends React.Component{
  render(){
    return(
      <div className="cart">
        <i className="fa fa-shopping-cart"></i>
        <button onClick={this.props.cartClickhandler.bind(this)}>cart ({this.props.cartCount})</button>
        {!this.props.signedIn ?<button onClick={this.props.loginButtonHandler.bind(this)}>log in</button>:null}
        {this.props.signedIn ?<button onClick={this.props.accountClickHandler.bind(this)}>Account</button>:null}
      </div>
    );
  }
}
