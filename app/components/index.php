
<!doctype html>
	<html lang="en">
	<head>
	<meta charset="utf-8">
	<title>Buy This Thing</title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.3/foundation.min.css" media="screen" title="no title" charset="utf-8">

	<script   src="https://code.jquery.com/jquery-3.1.0.slim.min.js" integrity="sha256-cRpWjoSOw5KcyIOaZNo4i6fZ9tKPhYYb6i5T9RSVJG8=" crossorigin="anonymous"></script>

	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	<script type="text/javascript">
	Stripe.setPublishableKey('pk_test_GwbZ42OeC9M7KRxd49t21LHC');
	</script>

	</head>
	<body>

		<form action="./buy.php" method="POST" id="payment-form" class="small-6 large-4 columns">
		  <span class="payment-errors"></span>

			<div class="form-row">
				<label>
					<span>First Name</span>
					<input type="text" name="first_name" class="">
				</label>
			</div>

		  <div class="form-row">
		    <label>
		      <span>Card Number</span>
		      <input type="text" size="20" data-stripe="number" class="card-number">
		    </label>
		  </div>

		  <div class="form-row small-6">
		    <label>
		      <span>Expiration (MM/YY)</span>
		      <input type="text" size="2" data-stripe="exp_month" class="small-6 columns card-expiry-month">
		    </label>
		    <span> / </span>
		    <input type="text" size="2" data-stripe="exp_year" class="small-6 columns card-expiry-year">
		  </div>

		  <div class="form-row">
		    <label>
		      <span>CVC</span>
		      <input type="text" size="4" data-stripe="cvc" class="card-cvc">
		    </label>
		  </div>

		  <input type="submit" class="submit" value="Submit Payment">
		</form>

		<script type="text/javascript">



		$(function() {
		  var $form = $('#payment-form');
		  $form.submit(function(event) {
		    // Disable the submit button to prevent repeated clicks:
		    $form.find('.submit').prop('disabled', true);

		    // Request a token from Stripe:
		    // Stripe.card.createToken($form, stripeResponseHandler);
				Stripe.card.createToken({
				  number: $('.card-number').val(),
				  cvc: $('.card-cvc').val(),
				  exp_month: $('.card-expiry-month').val(),
				  exp_year: $('.card-expiry-year').val()
				}, stripeResponseHandler);


		    // Prevent the form from being submitted:
		    return false;
		  });
		});






		function stripeResponseHandler(status, response) {
		  // Grab the form:
		  var $form = $('#payment-form');

		  if (response.error) { // Problem!

		    // Show the errors on the form:
		    $form.find('.payment-errors').text(response.error.message);
		    $form.find('.submit').prop('disabled', false); // Re-enable submission

		  } else { // Token was created!

		    // Get the token ID:
		    var token = response.id;

		    // Insert the token ID into the form so it gets submitted to the server:
		    $form.append($('<input type="hidden" name="stripeToken">').val(token));

		    // Submit the form:
		    $form.get(0).submit();

				console.log(token);
		  }
		};
		</script>

		<!-- <script type="text/javascript" src="js/buy.js"></script> -->
	</body>
</html>
