import React from 'react';

export default class Signup extends React.Component{
  // <div id="firebaseui-auth-container"></div>
  loginFormSubmit(e){
    e.preventDefault();
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    // console.log("email entered is " + email);
    // console.log("passwword entered is " + password);
    this.props.signupHandler(email,password);
    document.getElementById('email').value = "";
    document.getElementById('password').value = "";
  };

  render(){
    return(
      <div className="signupForm form">

        <div className="close">
          <button className="inner" onClick={this.props.signupButtonHandler.bind(this)}>Close</button>
        </div>

        <form onSubmit={this.loginFormSubmit.bind(this)}>

          <label>Username</label> 
          <input className="mdl-textfield__input" type="text" id="email" name="email" placeholder="Email" ref="email"/>

          <br/>
          <br/>

          <label>Password</label>
          <input className="mdl-textfield__input" type="password" id="password" name="password" placeholder="Password" ref="password"/>

          <br/>
          <br/>

         <button className="" id="sign-in" name="signup" onClick={this.loginFormSubmit.bind(this)}>Sign Up</button>

        </form>

      </div>
    );
  }
}
