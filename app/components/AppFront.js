import React from 'react';

import Header from "./Header.js";
import Cart from "./Cart.js";
import MerchendiseImage from "./MerchendiseImage.js";
import Products from "./Products.js";
import Login from "./Login.js";
import Signup from "./Signup.js";
import Footer from "./Footer.js";
import Full from "./Full.js";
import Checkout from "./Checkout.js";
import UserAccount from "./UserAccount.js";


var database = firebase.database();

export default class AppFront extends React.Component{
  //=============
  // Contstructor
  //=============
  constructor(){

    super();
    this.state = {
      products:[],
      productImage1:[],
      productImages:[],
      cartCache:[],

      homeUrl:"",
      galleryUrl:"",
      aboutUrl:"",
      contactUrl:"",

      cartCount:0,
      cartLoad:[],
      cartArray:null,

      productnameFull: "Loading...",
      productpriceFull: "Loading...",
      productDescriptionFull: "Loading...",
      addtoCartButton:"Add to Cart",

      fullPageHidden:true,
      productsHidden:false,
      cartHidden:true,
      loginHidden:true,
      signupHidden:true,
      checkoutHidden:true,
      userAccountHidden:true,

      checkoutDisabledState:false,
      checkoutButton:"checkout",

      firstLoop:true,
      forceReload: "",
      pageLoadedInit:false,

      signedIn:false,
      displayNameState: "Visitor",
      emailState:"",
      emailVerifiedState:"",
      photoURLState:"",
      userIdState:"",

      nameEdit:"",

      userOrdersState:[]

    };
    const self=this;
    //=============
    // Firebase value get
    //=============
    database.ref().on('value', snapshot => {
      this.setState({
        products:snapshot.val().products,
        homeUrl:snapshot.val().navigation.home,
        galleryUrl:snapshot.val().navigation.gallery,
        aboutUrl:snapshot.val().navigation.about,
        contactUrl:snapshot.val().navigation.contact
      });

    });

    //=============
    // Check if user is signed and if exists in the database
    //=============
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        // User is signed in.
        var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var uid = user.uid;
        var providerData = user.providerData;

        user.getToken().then(function(accessToken) {

          self.setState({
            displayNameState:displayName,
            emailState:email,
            emailVerifiedState:emailVerified,
            photoURLState:photoURL,
            signedIn:true,
            userIdState:uid
          });

          // check if user exists in database
          firebase.database().ref('users/').on('value', snapShot => {

            if( JSON.stringify(snapShot.val()).indexOf(email) === -1 ){
                // Add user to the database
                console.log("The user does not exist");
                self.addUserData(displayName, email, uid);
            } else {
              // Display user information
              console.log("The user does exists");
              firebase.database().ref('users/'+uid).on('value',snapShot =>{
                self.setState({
                  displayNameState:snapShot.val().display_name
                });
              });
              if(self.state.pageLoadedInit === false){
                self.showUserOrders();
              }
            }

          });
        });

      } else {
        // User is signed out.
        console.log("No user is signed in");
      }
    }, function(error) {
      console.log(error);
    });

  };//Constructor

  //=============
  // ComponentDidMount
  //=============
  componentDidMount(){
    this.setState({
      checkoutDisabledState:false
    })
  };

  //=============
  // Product Click / View product
  //=============
  productClickhandler(index){
    this.setState({
      productnameFull:this.state.products[index].name,
      productpriceFull:this.state.products[index].price,
      productDescriptionFull:this.state.products[index].description,
      productImages:this.state.products[index].images[0],
      fullPageHidden:false,
      productsHidden:true
    });
  };

  //=============
  // Add to Cart button click
  //=============
  addtoCart(myObj){
    var newArray = this.state.cartLoad; // returns [] until an item is added, then it's [object]
    var myObjString = JSON.stringify(myObj); // returns myObj as a string instead of an object
    var newArrayString = JSON.stringify(newArray); // returns newArray as a string instead of an object
    var myObjStringSplit = myObjString.split(","); // returns each value in the myObj variable as an array

    // console.log(myObjStringSplit);
    // console.log(myObjStringSplit[0]);
    // console.log(newArrayString.indexOf(myObjString,15));
    // console.log("newArray is " + newArrayString);
    // console.log("myObj is " + myObjString);

    if(newArrayString.indexOf(myObjStringSplit[0]) === -1 ){
      newArray.push(myObj);
        this.setState({
          cartLoad:newArray,
          cartCache:newArray,
          cartCount:this.state.cartCount +1,
          addtoCartButton: "Added"
        });
    } else{
      // Do Nothing
    };

  };

  //=============
  // Delete Cart Item
  //=============
  cartDeleteHandler(index){
    // console.log(index)
    var cartArray = this.state.cartLoad.slice();
    var cartArrayQuantity = cartArray[index].quantity;
    // console.log(cartArrayQuantity);
    cartArray.splice(index, 1);
    // console.log(cartArray);
    this.setState({cartLoad:cartArray});
    if(this.state.cartCount >= 0){
      this.setState({cartCount:this.state.cartCount-cartArrayQuantity});
    };
  };

  //=============
  // Go Back from product full page
  //=============
  closeFullHandler(){
    this.setState({
      fullPageHidden:true,
      productsHidden:false
    });
  };

  //=============
  // Cart Button Click
  //=============
  cartClickhandler(){
    this.state.cartHidden?
    this.setState({cartHidden:false})
    :
    this.setState({cartHidden:true})
  };

  //=============
  // Cart count increase & decrease
  //=============
  cartCountIncrease(){
    this.setState({cartCount:this.state.cartCount+1});
  }
  cartCountDecrease(){
    this.setState({cartCount:this.state.cartCount-1});
  }

  //=============
  // Login Handler
  //=============
  loginHandler(email,password){
    if (email.length < 4) {
      alert('Please enter an email address.');
      return;
    }
    if (password.length < 4) {
      alert('Please enter a password.');
      return;
    }
    // Sign in with email and pass.
    // [START authwithemail]
    firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // [START_EXCLUDE]
      if (errorCode === 'auth/wrong-password') {
        alert('Wrong password.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
      // [END_EXCLUDE]
    });
    // [END authwithemail]
  };

  //=============
  // Sign Out
  //=============
  signOutHandler(){
    if (firebase.auth().currentUser) {
      // [START signout]
      firebase.auth().signOut();
      // [END signout]
    }
    this.setState({
      displayNameState:"Visitor",
      emailState:"",
      signedIn:false,
      userAccountHidden:true
    })
  };

  //=============
  // Sign Up handler
  //=============
  signupHandler(email,password) {
    if (email.length < 4) {
      alert('Please enter an email address.');
      return;
    }
    if (password.length < 4) {
      alert('Please enter a password.');
      return;
    }
    // Sign in with email and pass.
    // [START createwithemail]
    firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // [START_EXCLUDE]
      if (errorCode == 'auth/weak-password') {
        alert('The password is too weak.');
      } else {
        alert(errorMessage);
      }
      console.log(error);
      // [END_EXCLUDE]
    });
    // [END createwithemail]
  }

  //=============
  // Login Button Click
  //=============
  loginButtonHandler(){
    if(this.state.loginHidden){
      this.setState({loginHidden:false,signupHidden:true,productsHidden:true});
    }else{
      this.setState({loginHidden:true,productsHidden:false})
    }
  };

  //=============
  // Account Button Handler
  //=============
  accountClickHandler(){
    this.state.userAccountHidden?
    this.setState({userAccountHidden:false})
    :
    this.setState({userAccountHidden:true})
  };

  //=============
  // Signup button click
  //=============
  signupButtonHandler(){
    if(this.state.signupHidden){
      this.setState({signupHidden:false,loginHidden:true});
    } else {
      this.setState({signupHidden:true,productsHidden:false});
    }
  };

  //=============
  // Checkout Button Handler
  //=============
  checkoutClickHandler(){
    if(this.state.checkoutHidden){
      this.setState({checkoutHidden: false, checkoutButton:"Close Checkout"})
    } else {
      this.setState({checkoutHidden: true , checkoutButton:"Checkout"})
    }
  };

  //=============
  // Hack force update
  //=============
  hackforceReload(){
    this.setState({forceReload:" "})
    console.log(this.state.forceReload);
  };

  //=============
  // Add User data to
  //=============
  addUserData(displayName, email, uid) {

        firebase.database().ref('users/'+uid).set({
          display_name: ""+this.state.displayNameState,
          email: ""+this.state.emailState,
          user_id:uid
        });

        console.log(displayName);
        console.log(email);
        console.log(uid);
  };

  //=============
  // Edit User Account Handler
  //=============
  editAccountDetails(userIdState,nameInputSet){
    this.setState({
      displayNameState:nameInputSet
    });
     firebase.database().ref('users/' + userIdState).on('value', snapShot =>{
       console.log(snapShot.val().display_name)
      //  console.log(nameInputSet);
     });

     firebase.database().ref('users/'+userIdState).update({
       display_name:nameInputSet
     });

  };

  //=============
  // Pull and display user orders
  //=============
  showUserOrders(){

    var ref = firebase.database().ref('users/'+this.state.userIdState+'/orders/');

    ref.on('value',snapShot => {
      var orderDB = snapShot.val();
      var orderDBKeys = Object.keys(orderDB);
      var userId = this.state.userIdState;

      for(var order = 0;order < orderDBKeys.length;order++){

        var orderArray = orderDB[orderDBKeys[order]];//get array of each order

        document.getElementById("orders").innerHTML += "<p>Order Code:<strong>"+ orderDBKeys[order] +"</strong></p>";

          for(var orderDetail = 0; orderDetail < orderArray.length; orderDetail++){
            document.getElementById("orders").innerHTML += "<p class='order'>"+
              "Product: "+ orderArray[orderDetail].name + "<br/>" +
              "Price: "+ orderArray[orderDetail].price + "<br/>" +
              "Quantity: "+ orderArray[orderDetail].quantity
              "</p><br/>";
          }//forLoop of orderDeatail
      }//forLoop of order
    });// order DB ref
    this.setState({pageLoadedInit:true,pageLoaded:true});

  };//addOrderHandler

  //=============
  // Add Order Handler
  //=============
  addOrderHandler(){

    var cartArray = this.state.cartLoad;

    firebase.database().ref('users/'+ this.state.userIdState +'/orders/').push(
      cartArray
    );

  };//addOrderHandler

  //=============
  // Render Stickermata Website
  //=============
  render(){
    return(
      <div id="websiteContainer">

        <UserAccount
        displayNameState={this.state.displayNameState}
        emailState={this.state.emailState}
        nameEdit={this.state.nameEdit}
        editAccountDetails={this.editAccountDetails.bind(this)}
        userIdState={this.state.userIdState}
        userIdState={this.state.userIdState}
        addOrderHandler={this.addOrderHandler.bind(this)}
        userOrdersState={this.state.userOrdersState}
        userAccountHidden={this.state.userAccountHidden}
        accountClickHandler={this.accountClickHandler.bind(this)}
        signOutHandler={this.signOutHandler.bind(this)}
        />

        <Header
          homeUrl={this.state.homeUrl} galleryUrl={this.state.galleryUrl} aboutUrl={this.state.aboutUrl} contactUrl={this.state.contactUrl} cartCount={this.state.cartCount}
          cartClickhandler={this.cartClickhandler.bind(this)}
          loginButtonHandler={this.loginButtonHandler.bind(this)}
          loginHidden={this.state.loginHidden}
          accountClickHandler={this.accountClickHandler.bind(this)}
          signedIn={this.state.signedIn}
        />

        <h1 className="welcomeMessage">Welcome, {this.state.displayNameState}</h1>

        {!this.state.loginHidden?
        <Login
          loginHandler={this.loginHandler.bind(this)}
          signedIn={this.state.signedIn}
          signOutHandler={this.signOutHandler.bind(this)}
          signupButtonHandler={this.signupButtonHandler.bind(this)}
          loginButtonHandler={this.loginButtonHandler.bind(this)}
        />
        :
        null
        }

        {!this.state.signupHidden?
        <Signup
          signupHandler={this.signupHandler.bind(this)}
          signupButtonHandler={this.signupButtonHandler.bind(this)}
        />
        :
        null
        }

        <Products
          products={this.state.products}
          productClickhandler={this.productClickhandler.bind(this)}
          productsHidden={this.state.productsHidden}
          productImages={this.state.productImages}
        />

        <Full
          productnameFull={this.state.productnameFull}
          productpriceFull={this.state.productpriceFull}
          productDescriptionFull={this.state.productDescriptionFull}
          fullPageHidden={this.state.fullPageHidden}
          closeFullHandler={this.closeFullHandler.bind(this)}
          addtoCart={this.addtoCart.bind(this)}
          cartLoad={this.props.cartLoad}
          productImages={this.state.productImages}
          addtoCartButton={this.state.addtoCartButton}
        />

        <hr className="invisible"/>

        <Footer />

        <Cart
          cartLoad={this.state.cartLoad}
          hackforceReload={this.hackforceReload.bind(this)}
          cartHidden={this.state.cartHidden}
          cartClickhandler={this.cartClickhandler.bind(this)}
          cartDeleteHandler={this.cartDeleteHandler.bind(this)}
          cartCountIncrease={this.cartCountIncrease.bind(this)}
          cartCountDecrease={this.cartCountDecrease.bind(this)}
          cartCount={this.state.cartCount}
          cartCache={this.state.cartCache}
          checkoutClickHandler={this.checkoutClickHandler.bind(this)}
          checkoutDisabledState={this.state.checkoutDisabledState}
          checkoutButton={this.state.checkoutButton}
          checkoutHidden={this.state.checkoutHidden}
          addOrderHandler={this.addOrderHandler.bind(this)}
          userIdState={this.state.userIdState}
        />

      </div>
    );
  }
}
