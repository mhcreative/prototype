import React from 'react';
import $ from 'jquery';

export default class Checkout extends React.Component{

  componentDidMount(){

    var $form = $('#payment-form');
    $form.find('.submit').prop('disabled', this.props.checkoutDisabledState);
  };

  submitFormHandler(e){

    var request;

    e.preventDefault();

		  var $form = $('#payment-form');

      var cartArray = this.props.cartLoad;

      var cartArrayString = JSON.stringify(cartArray);

      // console.log(JSON.stringify(cartArray,null,1));

      $form.find('.submit').prop('disabled', true);

      const self = this;

         Stripe.createToken(e.target, function(status, response, cartArray) {
           if (response.error) {
               $form.find('.payment-errors').text(response.error.message);
               $form.find('.submit').prop('disabled', false); // Re-enable submission
           }
           else {

            let token = response.id;

            $form.append($('<input type="hidden" name="stripeToken">').val(token));

            console.log(token);

            console.log("Processing Payment...");

            $.ajax({
               method: 'POST',
               url: './components/buy.php',
               data : {
                 number: $('.card-number').val(),
                 cvc: $('.card-cvc').val(),
                 exp_month: $('.card-expiry-month').val(),
                 exp_year: $('.card-expiry-year').val(),
                 first_name: $('.first_name').val(),
                 email: $('.email').val(),

                 address_line1: $('.address_line1').val(),
                 address_city: $('.address_city').val(),
                 address_country: $('.address_country').val(),
                 address_zip: $('.address_zip').val(),

                 token: token,
                 metadata:{order:cartArrayString}
               },
              //  dataType: 'json',
               success: function(data) {
                     console.log("success");

                     $('.card-number').val("");
                     $('.card-cvc').val("");
                     $('.card-expiry-month').val("");
                     $('.card-expiry-year').val("");
                     $('.address_line1').val("");
                     $('.email').val("");

                     $('.address_line1').val("");
                     $('.address_city').val("");
                     $('.address_country').val("");
                     $('.address_zip').val("");

               },
               error: function(data,textStatus) {
                     console.log("Ajax Error!");
                    //  console.log(data);
                    //  console.log(textStatus);
               }
             });//$.ajax

           };

         });//Stripe.createToken
         var userIdState = this.props.userIdState;
         this.props.addOrderHandler(userIdState,cartArray);

  };

  render(){
    return(

      <div className={this.props.cartHidden || this.props.cartCount === 0 ? 'checkout hidden' : 'checkout'}>
        <form id="payment-form" className="small-6 large-6 columns" onSubmit={this.submitFormHandler.bind(this)}>
        <h1 className="welcomeMessage">CHECKOUT</h1>
          <span className="payment-errors"></span>

          <div className="form-row">
            <label>
              <span>First Name</span>
              <input type="text" className="first_name" data-stripe="first_name"/>
            </label>
          </div>

          <div className="form-row">
            <label>
              <span>Email</span>
              <input type="text" className="email" data-stripe="email"/>
            </label>
          </div>

          <div className="form-row">
            <label>
              <span>Address</span>
              <input type="text" className="address_line1" data-stripe="address_line1" placeholder="123 Fake Street, suburb"/>

              <span>City</span>
              <input type="text" className="address_city" data-stripe="address_city" placeholder="city"/>

              <span>address_country</span>
              <input type="text" className="address_country" data-stripe="address_country" placeholder="Country"/>

              <span>Postcode</span>
              <input type="text" className="address_zip" data-stripe="address_zip" placeholder="Postcode"/>
            </label>
          </div>

          <div className="form-row">
            <label>
              <span>Card Number</span>
              <input type="text" size="20" data-stripe="number" className="card-number" />
            </label>
          </div>

          <label>EXPIRATION (MM/YY)</label>
          <div className="input-group large-6 exp">
            <span className="input-group-label">MONTH</span>
            <input type="text" size="2" data-stripe="exp_month" className="small-6 columns card-expiry-month" />
          </div>

          <div className="input-group large-6 exp">
            <span className="input-group-label">YEAR</span>
            <input type="text" size="2" data-stripe="exp_year" className="small-6 columns card-expiry-year" />
          </div>

          <div className="form-row">
            <label>
              <span>CVC</span>
              <input type="text" size="4" data-stripe="cvc" className="card-cvc" />
            </label>
          </div>

          <input type="submit" className="submit" value="Submit Payment" />
        </form>
      </div>

    );
  }
}
