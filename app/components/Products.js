import React from 'react';

export default class Products extends React.Component{
  constructor(){
    super();
      this.state = {
        productsLoaded:false
      }
  };

  handleImageLoaded(){
    this.setState({
      productsLoaded:true
    })

  };

  render(){
    const listProducts = this.props.products.map((item,index)=>{
        return(
        <div key={index} className="product" onClick={this.props.productClickhandler.bind(this,index)}>
          <div className="inner">
            <div className="image">
              <div className="productsImageList">
                <img src={item.images[0].image1} onLoad={this.handleImageLoaded.bind(this)} />
              </div>
            </div>
            <h1 className="description">{item.name}</h1>
            <h2 className="price">${item.price}</h2>
          </div>
        </div>
      );
    });

    return(
      <div className={this.props.productsHidden?"merchandise hidden":"merchandise"}>
        {this.state.productsLoaded ? null :<div className="preloader"><img src="./img/preloader.gif" /></div>}
        {listProducts}
      </div>
    );
  }
}
