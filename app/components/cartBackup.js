import React from 'react';

export default class Cart extends React.Component{
  quantityAdd(item){
    item.quantity++
    this.props.hackforceReload();
    this.props.cartCountIncrease();
  };
  quantityMinus(item,index){
    if(item.quantity === 1){
      this.props.cartDeleteHandler(index);
    }else{
      item.quantity--
      this.props.cartCountDecrease(item);
    };
    this.props.hackforceReload();
  };
  render(){
    var listCartItems =
    this.props.cartLoad.map((item,index)=>{
      return(
        <li key={index} className="cartItem">
        {item.name}<br/>
        {item.price}<br/>
        <button onClick={this.quantityMinus.bind(this,item,index)}>-</button><span className="quantityCount">{item.quantity}</span><button onClick={this.quantityAdd.bind(this,item)}>+</button>
        <button className="cartDelete" onClick={this.props.cartDeleteHandler.bind(this,index)}>Delete this item</button>
        </li>
      )
    });

    return(
      <div className={this.props.cartHidden ?'cartFull hidden' : 'cartFull'}>
      <hr/>

      <div>
        {this.props.cartCount > 0 ? <span>This is the cart</span> : <span>Nothing in the cart</span>}
        <button className="cartClose" onClick={this.props.cartClickhandler.bind(this)}>close</button>
      </div>

      {listCartItems}

      <button onClick={this.props.checkoutClickHandler.bind(this)}>{this.props.checkoutButton}</button>

      </div>
    );
  }
}
