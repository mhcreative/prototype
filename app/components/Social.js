import React from 'react';

export default class Social extends React.Component{
  render(){
    return(
      <div className="social">
          <ul className="inner">
           <li><a href="#"><i className="fa fa-facebook-official"></i></a></li>
            <li><a href="#"><i className="fa fa-instagram"></i></a></li>
        </ul>
      </div>
    );
  }
}
