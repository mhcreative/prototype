import React from 'react';

export default class Full extends React.Component{
  addToCartClick(){
    var myObj = {
      "name":this.props.productnameFull,
       "price":this.props.productpriceFull,
       "quantity":1
    }
    this.props.addtoCart(myObj);
    // console.log(this.props.productImages[0].image1);
  };
  render(){
    return(
      <div className={this.props.fullPageHidden ?'productFull hidden' : 'productFull'}>

        <div className="close">
          <button className="inner" onClick={this.props.closeFullHandler.bind(this)}>Back</button>
        </div>

        <div className="image">
          <div className="inner">

            <div className="slide">
              <img src={this.props.productImages.image1} />
            </div>

          </div>
        </div>

          <div className="product">
          <h1 className="productFullName">{this.props.productnameFull}</h1>
            <div className="inner">
              <h2 className="price">${this.props.productpriceFull}</h2>

              <button className="addtoCartBtn" onClick={this.addToCartClick.bind(this)}>Add to Cart</button>

              <h1 className="description">{this.props.productDescriptionFull}</h1>
            </div>
          </div>
        </div>
    );
  }
}
