import React from 'react';

export default class Login extends React.Component{
  // <div id="firebaseui-auth-container"></div>
  loginFormSubmit(e){
    e.preventDefault();
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    // console.log("email entered is " + email);
    // console.log("passwword entered is " + password);
    this.props.loginHandler(email,password);
    document.getElementById('email').value = "";
    document.getElementById('password').value = "";
  };

  render(){
    return(
      <div className="loginForm form">
      <div className="close">
        <button className="inner" onClick={this.props.loginButtonHandler.bind(this)}>Close</button>
      </div>
      {!this.props.signedIn ?<form onSubmit={this.loginFormSubmit.bind(this)}>
         <label>Username</label>
         <input className="mdl-textfield__input" type="text" id="email" name="email" placeholder="Email" ref="email"/>

         <br/>
         <br/>

         <label>Password</label>
         <input className="mdl-textfield__input" type="password" id="password" name="password" placeholder="Password" ref="password"/>

         <br/>
         <br/>

        <button className="" id="sign-in" name="signin" onClick={this.loginFormSubmit.bind(this)}>Sign In</button>

      </form>: null}

      {this.props.signedIn ? <button className="" id="sign-out" name="signin" onClick={this.props.signOutHandler.bind(this)}>Sign Out</button>

      : null}

     <button className="" className="signup cta" name="signup" onClick={this.props.signupButtonHandler.bind(this)}>Don't have an account? Signup here</button>

      </div>
    );
  }
}
