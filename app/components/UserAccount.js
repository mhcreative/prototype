import React from 'react';

export default class UserAccount extends React.Component{
  editNameInput(e){
    e.preventDefault();
    var nameInputSet = document.getElementById("nameInput").value;
    var userIdState = this.props.userIdState;
    // console.log(nameInputSet);
    if(nameInputSet != ""){
      this.props.editAccountDetails(userIdState,nameInputSet);
    }
    document.getElementById("nameInput").value = "";

  };//editNameInput

  openModal(){
    // Get the modal
    var modal = document.getElementById('myModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    modal.style.display = "block";

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            // modal.style.display = "none";
            this.props.accountClickHandler();
        }
    };

  };

  closeModal(){
      var modal = document.getElementById('myModal');
      // modal.style.display = "none";
      this.props.accountClickHandler();
  };

  render(){

    return(
      <div>

        <div id="myModal" className={this.props.userAccountHidden ?'modal hidden' : 'modal'}>
          <div className="modal-content">
            <span onClick={this.closeModal.bind(this)}className="close">x</span>
            <div className="signout">
              <button className="inner" onClick={this.props.signOutHandler.bind(this)}>Sign out</button><br/>
            </div>
            <ul>
            <li>name:{this.props.displayNameState}
            <br/>
            <form onSubmit={this.editNameInput.bind(this)}>
            edit name: <input id="nameInput" text="text" ref="nameInput" />
            <button>Edit</button>
            </form>
            </li>
            <li>email:{this.props.emailState}</li>
            <li>user ID: {this.props.userIdState}</li>
            </ul>
            <h3 className="ordersHeader">Orders:</h3><br/>
            <div id="orders"></div>
          </div>
        </div>
      </div>
    );
  }
}
