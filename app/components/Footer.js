import React from 'react';

import Login from './Login.js'

export default class Footer extends React.Component{
  render(){
    return(
      <footer>
        <ul className="footerNav">
          <li><a href="#">shop</a></li>
          <li><a href="#">gallery</a></li>
          <li><a href="#">about</a></li>
          <li><a href="#">contact</a></li>
        </ul>

        <ul className="disclaimers">
          <li><a href="#">terms & conditions</a></li>
          <li><a href="#">shipping & returns</a></li>
        </ul>



      </footer>
    );
  }
}
